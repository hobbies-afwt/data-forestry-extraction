import csv
import pandas as pd

def data_analysis():
    df = pd.read_excel('./static/input/archivo.xlsx', engine='openpyxl')
    filtered_df = df[df['Title'].str.contains("Arbustal abierto", na=False)]
    description_array = filtered_df['Description'].tolist()
    return description_array

def data_extraction(entradas, species_list):
    results = {}
    for w in range(len(entradas)):
        entrada = str(entradas[w]).split(" ")
        results[w] = {}
        for species in species_list:
            i = 0
            while i < len(entrada):
                if entrada[i] == species:
                    if species == "Chilco":
                        i += 2
                    elif species == "Morella pubescens":
                        i += 1
                    j = i + 1
                    species_values = []
                    while j < len(entrada) and entrada[j] not in species_list:
                        if ':' in entrada[j]:
                            try:
                                value = int(entrada[j+1].replace('%', ''))
                                if value < 200:
                                    species_values.append(entrada[j+1])
                            except ValueError:
                                pass
                        j += 1
                    if species_values:
                        results[w][species] = species_values
                    break
                i += 1
    return results

def create_csv(results):
    with open('./static/output/species_values.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        max_values = max([len(val) for sublist in results.values() for val in sublist.values()])
        
        headers = ['Species']
        for i in range(max_values):
            headers.append(f"Value {i+1}")
        writer.writerow(headers)

        for idx, data in results.items():
            for species, values in data.items():
                row = [species]
                row.extend(values)
                while len(row) < max_values + 1:  
                    row.append('')
                writer.writerow(row)


def create_excel(results):
    df = pd.DataFrame.from_dict(results, orient='index').transpose()
    df.to_excel('./static/output/species_values.xlsx', index=False, engine='openpyxl')

species_list = ["Dodonaea", "Chilco", "Morella", "Morella pubescens"]
results = data_extraction(data_analysis(),species_list)
create_csv(results)
create_excel(results)